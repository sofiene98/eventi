import 'package:eventiii/view/Games.dart';
import 'package:eventiii/view/cart.dart';
import 'package:eventiii/view/categories.dart';
import 'package:eventiii/view/edit_profile.dart';
import 'package:eventiii/view/first_screen.dart';
import 'package:eventiii/view/home.dart';
import 'package:eventiii/view/profile_ghazi.dart';
import 'package:eventiii/view/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:slide_countdown/slide_countdown.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:eventiii/view/Sign_Up.dart';
import 'view/multiple_choice.dart';
import 'view/register.dart';
import 'package:eventiii/view/sign_in.dart';
import 'package:eventiii/view/profile_ghazi.dart';
import 'package:image/image.dart' as ImageProcess;


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Key? get key => super.key;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        // Remove the debug banner
        debugShowCheckedModeBanner: false,
        routes: {
          'LandingScreen': (context) => FirstScreen(),
          'HomePage': (context) => Eventi(),
          'SignUpPage': (context) => SignUp(),
          'Register': (context) => Register(),
          'SignIn': (context) => SignIn(),
          'Games': (context) => Games(),
          'Profile': (context) => Profile(),
          'Categories': (context) => Categories(),
          'Multiple choice': (context) => MC(),
          'CartPage': (context) => CartPage(),
          'EditProfile': (context) => EditProfile()
          //'Test' :(context) => ProfilePictureSelect(key: key, isMobile: true, userId: '0')
        },
        theme: ThemeData(fontFamily: 'Montserrat'),
        home: Eventi());
  }
}

class Eventi extends StatefulWidget {
  @override
  _EventiState createState() => _EventiState();
}

// Home Page
class _EventiState extends State<Eventi> {
  int _current = 0;
  int _currentIndex = 0;
  static const Color _bottonnavbar = Color(0xFFFFFBFA);

  final controller = PageController(viewportFraction: 0.8, keepPage: true);
  final CarouselController _controller = CarouselController();
  ScrollController _rrectController = ScrollController();

  List<Widget> screens = [
    new Home(),
    new Categories(),
    new Games(),
    new CartPage(),
    new Profile()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        //mouseCursor: SystemMouseCursor._(kind: 'click') ,
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        backgroundColor: Color(0XFF75CFCF),
        selectedItemColor: Color(0xFFFFFBFA),
        unselectedItemColor: Color(0xFFFFFBFA).withOpacity(0.60),
        onTap: (value) {
          setState(() {
            _currentIndex = value;
            screens[_currentIndex];
          });
        },
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.format_list_bulleted), label: 'Categories'),
          BottomNavigationBarItem(icon: Icon(Icons.games), label: 'Games'),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart), label: 'Cart'),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            label: 'Profile',
          ),
        ],
      ),
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
                icon: Icon(Icons.menu));
          },
        ),
        backgroundColor: Color(0XFF75CFCF),
        actions: [
          // Navigate to the Search Screen
          IconButton(
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => SearchPage())),
              icon: Icon(Icons.search))
        ],
      ),
      body: screens[_currentIndex],

      // Navigator.push(context,
      //                   new MaterialPageRoute(builder: (context) => Eventi() ));
    );
  }
}

// Search Page
class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0XFF75CFCF),
          // The search area here
          title: Container(
            width: double.infinity,
            height: 40,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(5)),
            child: Center(
              child: TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        /* Clear the search field */
                      },
                    ),
                    hintText: 'Search...',
                    border: InputBorder.none),
              ),
            ),
          )),
    );
  }
}
