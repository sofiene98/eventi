class annonce {
  String categorie;
  String dateA;
  String description;
  double discount;
  int duration;
  int idA;
  String imageA;
  String location;
  String nomAnnonce;
  int prix;

  annonce(
      {required this.categorie,
        required this.dateA,
        required this.description,
        required this.discount,
        required this.duration,
        required this.idA,
        required this.imageA,
        required this.location,
        required this.nomAnnonce,
        required this.prix});

  factory annonce.fromJson(Map<String, dynamic> json) {
    return annonce(categorie : json['categorie'],
    dateA : json['dateA'],
    description : json['description'],
    discount : json['discount'],
    duration : json['duration'],
    idA : json['idA'],
    imageA : json['imageA'],
    location : json['location'],
    nomAnnonce : json['nom_annonce'],
    prix : json['prix']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['categorie'] = this.categorie;
    data['dateA'] = this.dateA;
    data['description'] = this.description;
    data['discount'] = this.discount;
    data['duration'] = this.duration;
    data['idA'] = this.idA;
    data['imageA'] = this.imageA;
    data['location'] = this.location;
    data['nom_annonce'] = this.nomAnnonce;
    data['prix'] = this.prix;
    return data;
  }
}