class offresauvegardee {
  int idofs;
  int idc;

  offresauvegardee({required this.idofs, required this.idc});

  factory offresauvegardee.fromJson(Map<String, dynamic> json) {
    return offresauvegardee(idofs: json['idofs'], idc: json['idc']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idofs'] = this.idofs;
    data['idc'] = this.idc;
    return data;
  }
}