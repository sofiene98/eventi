class orderitem {
  int ido;
  int idc;
  int idcp;
  String dateO;
  int state;

  orderitem(
      {
        required this.ido,required this.idc, required this.idcp,required this.dateO,required this.state
      }
          );

  factory orderitem.fromJson(Map<String, dynamic> json) {
    return orderitem(
        ido : json['ido'],
        idc:json['idc'],
        idcp:json['idcp'],
        dateO:json['dateO'],
        state: json['state']
    );
  }


}