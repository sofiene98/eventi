import 'dart:convert';

import 'package:eventiii/view/Band.dart';
import 'package:eventiii/view/private_venues.dart';
import 'package:eventiii/view/transport.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:side_navigation/api/side_navigation_bar.dart';
import 'package:side_navigation/api/side_navigation_bar_item.dart';
import 'package:http/http.dart' as http;

import 'Food.dart';

//Search page
class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0XFF75CFCF),
          // The search area here
          title: Container(
            width: double.infinity,
            height: 40,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(5)),
            child: Center(
              child: TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        /* Clear the search field */
                      },
                    ),
                    hintText: 'Search...',
                    border: InputBorder.none),
              ),
            ),
          )),
    );
  }
}

class Categories extends StatefulWidget {
  const Categories({Key? key}) : super(key: key);

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {


  late List<String> listofItems=[]; // b64
  late int selectedIndex = 2;
  late List<String> nameOfItems=[];
  late List<String> descriptionofItems=[];
  late List<String> listDate=[];


  /*
  (() async {

      final response = await http.get(Uri.parse('http://10.0.2.2:5000/annonces'),headers: {
        "Accept": "application/json",
        "Access-Control-Allow-Origin": "*"});
      setState(() {
        _base64=json.decode(response.body)[0]['imageA'];

        for(int i=0;i<2;i++){

          var tmpimage64=json.decode(response.body)[i]['imageA']; //image
          var nomAnnonce=json.decode(response.body)[i]['nom_annonce']; //image
          var dateA=json.decode(response.body)[i]['dateA']; //image
          var desc=json.decode(response.body)[i]['description']; //image



          listofItems.add(tmpimage64);
          nameOfItems.add(nomAnnonce);
          descriptionofItems.add(desc);
          listDate.add(dateA);

        }

        int i=0;



        // print(" ahaayaaaaaaaaa "+listofItems[0]);

      });

    })();
   */


  List <Widget> categories =[  new Band(), new PV("0"), new Food(), new Transport(),PV("0")];//Add the categories widgets



  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: Colors.grey[200],
      // appBar: AppBar(
      //
      //   leading: Builder(
      //     builder: (BuildContext context) {
      //       return IconButton(
      //           onPressed: () {
      //             Navigator.pop(context);
      //           },
      //           icon: Icon(Icons.arrow_back));
      //     },
      //   ),
      //   backgroundColor: Color(0XFF75CFCF),
      //   actions: [
      //     // Navigate to the Search Screen
      //     IconButton(
      //         onPressed: () => Navigator.of(context)
      //             .push(MaterialPageRoute(builder: (_) => SearchPage())),
      //         icon: Icon(Icons.search))
      //   ],
      // ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            child: NavigationRail(
              groupAlignment: 0.0,
              selectedIndex: selectedIndex,
              onDestinationSelected: (int index) {
                setState(() {
                  selectedIndex=index;
                  print(selectedIndex);
                });
              },
              labelType: NavigationRailLabelType.selected,
              destinations: <NavigationRailDestination> [
                NavigationRailDestination(
                  padding: EdgeInsets.all(5.0),
                  selectedIcon: Icon(FontAwesome5.music),
                  icon: Icon(FontAwesome5.music),

                  label: Column(
                    children: [

                      InkWell(
                          child:  Text(
                            'Band',
                            style: TextStyle(
                                fontSize: 10.0,
                                fontWeight: FontWeight.bold
                            ),
                          ),

                          onTap: ()  {
                            setState(() {

                              print(selectedIndex);
                            });




                          }



                      ),


                    ],

                  ),



                ),
                NavigationRailDestination(



                  icon: Icon(Icons.place),
                  label: Column(
                    children: [

                      InkWell(
                          child:  Text(
                            'Private',
                            style: TextStyle(
                                fontSize: 10.0,
                                fontWeight: FontWeight.bold
                            ),
                          ),

                          onTap: () async {

                            setState(() {
                              selectedIndex=1;

                            });

                            print(selectedIndex);



                          }



                      ),

                      InkWell(

                        onTap: ()  {
                          selectedIndex=1;
                          print(selectedIndex);



                        },
                        child:Text(
                          'Venues',
                          style: TextStyle(
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      )
                    ],

                  ),


                ),

                NavigationRailDestination(
                    icon: Icon(Icons.restaurant),

                    label: Column(
                      children: [
                        Text(
                          'Food',
                          style: TextStyle(
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        Text(
                          'Services',
                          style: TextStyle(
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold
                          ),
                        )
                      ],
                    )),
                NavigationRailDestination(
                    icon: Icon(FontAwesome5.car_side),
                    label: Text(
                      'Transport',
                      style: TextStyle(
                          fontSize: 10.0,
                          fontWeight: FontWeight.bold
                      ),
                    )),
                NavigationRailDestination(
                    icon: Icon(Icons.more_horiz),
                    label: Column(
                      children: [
                        Text(
                          'Other',
                          style: TextStyle(
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        Text(
                          'Services',
                          style: TextStyle(
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold
                          ),
                        )
                      ],
                    ))
              ],

            ),
          ),
          VerticalDivider(
            thickness: 3,
            width: 2,
          ),


          Expanded(
              child: Center(
                  child: categories[selectedIndex]
              ))
        ],
      ),
    );
  }
}
