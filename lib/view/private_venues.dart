import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PV extends StatefulWidget {
  final categorie;

  const PV(this.categorie, {Key? key}) : super(key: key);

  @override
  _PVState createState() => _PVState();
}

class _PVState extends State<PV> {
  late String categorie;

  late List<String> listofItems = []; // b64

  late List<String> nameOfItems = ["Top Happiness", "Sophie Wedding Venue"];
  late List<String> descriptionofItems = ["Nice Experience ", "Smooth"];
  late List<int> listIdItems = [6, 7];
  late List<String> listDate = ["24/12/2021", "25/01/2022"];
  late List<Uint8List> bytes = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        'First Typee ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    //First Type
                    Divider(
                      height: 10,
                      indent: 15.0,
                      endIndent: 15.0,
                      thickness: 3.5,
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                            Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset("assets/images/Salle.jpg"),
                            title: Text(nameOfItems[0],
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () async {
                                    try {
                                      int idcommande = 1;
                                      final response = await http.get(
                                          Uri.parse(
                                              'http://10.0.2.2:5000/addCommand?id=6&idcommande=' +
                                                  idcommande.toString()),
                                          headers: {
                                            "Accept": "application/json",
                                            "Access-Control-Allow-Origin": "*",
                                            "Keep-Alive": "timeout=25, max=1000"
                                          });
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(40)),
                                            elevation: 16,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(50),
                                                  color: Colors.lightGreen),
                                              child: ListView(
                                                shrinkWrap: true,
                                                children: <Widget>[
                                                  SizedBox(height: 20),
                                                  Center(
                                                      child: Text(
                                                          'Check your cart',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold))),
                                                  SizedBox(height: 20),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                      //Navigator.pushNamed(context, 'HomePage');
                                    } on Exception {}
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Second Type ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Divider(
                      height: 10,
                      indent: 15.0,
                      endIndent: 15.0,
                      thickness: 3.5,
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                            Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset("assets/images/S4.png"),
                            title: Text(nameOfItems[1],
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () async {
                                    int idcommande = 2;
                                    try {
                                      final response = await http.get(
                                          Uri.parse(
                                              'http://10.0.2.2:5000/addCommand?id=6&idcommande=' +
                                                  idcommande.toString()),
                                          headers: {
                                            "Accept": "application/json",
                                            "Access-Control-Allow-Origin": "*",
                                            "Keep-Alive": "timeout=25, max=1000"
                                          });
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(40)),
                                            elevation: 16,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(50),
                                                  color: Colors.lightGreen),
                                              child: ListView(
                                                shrinkWrap: true,
                                                children: <Widget>[
                                                  SizedBox(height: 20),
                                                  Center(
                                                      child: Text(
                                                          'Check your cart',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold))),
                                                  SizedBox(height: 20),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    } on Exception {
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(40)),
                                            elevation: 16,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(50),
                                                  color: Colors.redAccent),
                                              child: ListView(
                                                shrinkWrap: true,
                                                children: <Widget>[
                                                  SizedBox(height: 20),
                                                  Center(
                                                      child: Text(
                                                          'Server is down',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold))),
                                                  SizedBox(height: 20),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    }

                                    //Navigator.pushNamed(context, 'HomePage');
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                            Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/Salle.jpg'),
                            title: Text("Band N° 3",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, 'HomePage');
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ]))
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Conference Rooms ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Divider(
                      height: 10,
                      indent: 15.0,
                      endIndent: 15.0,
                      thickness: 3.5,
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                        Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/CS 3.jpg'),
                            title: Text("MHK Conference Room",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                        Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/CS 4.jpg'),
                            title: Text("C4 Coworking Space",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])
                    )
                  ],
                ),
              ),
            ),
            TextButton.icon(
                onPressed: () {

                  Navigator.pushNamed(context, 'AddOffer');
                },
                icon: Icon(Icons.add),
                label: Text(
                  'Add a new offer',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                ))
          ],
        ),
      ),
    );
  }
}
