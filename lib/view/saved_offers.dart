import 'dart:convert';

import 'package:eventiii/view/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome5_icons.dart';

import '../main.dart';
import '../utils/CustomTextStyle.dart';
import '../utils/CustomUtils.dart';
import 'package:http/http.dart' as http ;

import 'Games.dart';
import 'cart.dart';
import 'categories.dart';
import 'home.dart';
class SavedOffers extends StatefulWidget {
  const SavedOffers({Key? key}) : super(key: key);

  @override
  _SavedOffersState createState() => _SavedOffersState();
}

class _SavedOffersState extends State<SavedOffers> {


  late List<String> nomAnnonce=[];
  late List<String> categoriee=[];
  late List<double> prix=[];
  late int lengthres=0;

  late List<String> listofImages=[];

  late double total=0;

  int _currentIndex =4;

  List<Widget> screens =[new Eventi(),new Categories(),new Games(),new CartPage(),new Profile()];







  Future<void> fetchCommands() async {





    String url="http://10.0.2.2:5000/getAnnonceSauv?idSession=6&mycommands=0";

    print(url);
    final response = await http.get(
        Uri.parse(url), headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*"});



    print(response.body.length);
    setState(() {
      lengthres=response.body.length;
    });


    for(int i=0;i<response.body.length;i++) {
      var nomAnnoncee = json.decode(response.body)[i]['nom_annonce'];
      var categorie = json.decode(response.body)[i]['categorie'];
      //var price = json.decode(response.body)[i]['prix'];
      setState(() {
        nomAnnonce.add(nomAnnoncee);
        categoriee.add(categorie);
                  });



                                            }








  }


  @override
  void initState() {
    super.initState();

    try {
      fetchCommands();
    }
    on Exception{

    }


  }



  @override
  Widget build(BuildContext context)  {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Offres sauvegardés',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold
          ),
        ),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back));
          },
        ),
        backgroundColor: Color(0XFF75CFCF),
      ),
      bottomNavigationBar: BottomNavigationBar(
        //mouseCursor: SystemMouseCursor._(kind: 'click') ,
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        backgroundColor: Color(0XFF75CFCF),
        selectedItemColor: Color(0xFFFFFBFA),
        unselectedItemColor: Color(0xFFFFFBFA).withOpacity(0.60) ,
        onTap: (value) {
          setState(() {
            _currentIndex=value;
           // screens[_currentIndex];
            Navigator.push(context, new MaterialPageRoute(builder: (context) => screens[_currentIndex] ));

            //screens[_currentIndex];

          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.format_list_bulleted),
              label: 'Categories'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.games),
              label: 'Games'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart),
              label: 'Cart'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            label: 'Profile',
          ),
        ],
      ),
      body: Builder(
        builder: (context) {
          return ListView(
            children: <Widget>[
              createCartList(),
            ],
          );
        },
      ),
    );
  }




  createSubTitle() {
    return Container(
      alignment: Alignment.topLeft,
      child: Text(
        "Total(3) Items",
        style: CustomTextStyle.textFormFieldBold
            .copyWith(fontSize: 12, color: Colors.grey),
      ),
      margin: EdgeInsets.only(left: 12, top: 4),
    );
  }

  createCartList()   {





    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemBuilder: (context, position)  {
        // base64, nomAnnonce , prix





        return createCartListItem(nomAnnonce[position],categoriee[position]);






      },
      itemCount: nomAnnonce.length,
    );
  }

  createCartListItem(String nomAnnonce,String categorie) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Column(
            children: [
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 8, left: 8, top: 8, bottom: 8),
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                        //color: Colors.blue.shade200,
                        image: DecorationImage(
                            image: AssetImage("assets/images/S4.png"))),
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(right: 8, top: 4),
                            child: Text(
                              nomAnnonce,
                              maxLines: 2,
                              softWrap: true,
                              style: CustomTextStyle.textFormFieldSemiBold
                                  .copyWith(fontSize: 14),
                            ),
                          ),
                          Utils.getSizedBox(height: 6, width: 0.0),
                          Text(
                            categorie,
                            style: CustomTextStyle.textFormFieldRegular
                                .copyWith(color: Colors.grey, fontSize: 14),
                          )
                        ],
                      ),
                    ),
                    flex: 100,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          IconButton(
                              onPressed: () {},
                              icon: Icon(FontAwesome5.heart)),
                          IconButton(
                              onPressed: () {},
                              icon: Icon(FontAwesome5.trash_alt)),
                        ],
                      )
                  )
                ],
              ),
            ],
          ),
        )
      ],
    );
  }
}