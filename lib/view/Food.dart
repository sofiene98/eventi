import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:typed_data';

import 'package:http/http.dart' as http;

import 'add_offer.dart';

class Food extends StatefulWidget {
  const Food({Key? key}) : super(key: key);

  @override
  _FoodState createState() => _FoodState();
}

class _FoodState extends State<Food> {
  late String categorie;

  late List<String> listofItems = []; // b64

  late List<String> nameOfItems = ["Turkish Food", "Tunisian Food"];
  late List<String> descriptionofItems = ["Miam Miam ", "Delicious"];
  late List<int> listIdItems = [6, 7];
  late List<String> listDate = ["24/12/2021", "25/01/2022"];
  late List<Uint8List> bytes = [];

  @override
  void initState() {
    super.initState();
    //inita();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [
            // Padding(
            //   padding: const EdgeInsets.all(8.0),
            //   child: Container(
            //     color: Colors.white,
            //     child: Column(
            //       children: [
            //         Container(
            //           padding: EdgeInsets.all(10.0),
            //           alignment: Alignment.topLeft,
            //           child: Text(
            //               'Recommended ',
            //             style: TextStyle(
            //               fontWeight: FontWeight.bold,
            //               fontSize: 20.0,
            //             ),
            //           ),
            //         ),
            //         Divider(
            //           height: 10,
            //           indent: 15.0,
            //           endIndent: 15.0,
            //           thickness: 3.5,
            //         ),
            //         Card(
            //           margin: EdgeInsets.all(0.0),
            //             child: Column(
            //                 mainAxisSize: MainAxisSize.min,
            //                 children: [
            //                   ListTile(
            //                     contentPadding: EdgeInsets.all(10.0),
            //                     leading: Image.asset('assets/images/Salle.jpg'),
            //                     title: Text("Boudourou Band",
            //                         style: TextStyle(
            //                             fontSize: 15,
            //                             fontWeight: FontWeight.bold
            //                         )
            //                     ),
            //                   ),
            //                   ButtonBar(
            //                     alignment: MainAxisAlignment.spaceAround,
            //                     children: [
            //                       TextButton(
            //                           onPressed: () {
            //                             Navigator.pushNamed(context, 'HomePage');
            //                           },
            //                           child:Text(
            //                             'Explore Plans',
            //                             style: TextStyle(
            //                               fontSize: 20.0,
            //                               fontWeight: FontWeight.normal
            //                             ),)
            //                       ),
            //                     ],
            //                   ),
            //                 ]     )
            //
            //         ),
            //
            //       ],
            //
            //     ),
            //   ),
            // ), aa

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        nameOfItems[0],
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    //First Type
                    Divider(
                      height: 10,
                      indent: 15.0,
                      endIndent: 15.0,
                      thickness: 3.5,
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                            Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset("assets/images/f1.png"),
                            title: Text(descriptionofItems[0],
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () async {
                                    // Navigator.pushNamed(context, 'CartPage');

                                    //

                                    try {
                                      int idcommande = 6;
                                      final response = await http.get(
                                          Uri.parse(
                                              'http://10.0.2.2:5000/addCommand?id=6&idcommande=' +
                                                  idcommande.toString()),
                                          headers: {
                                            "Accept": "application/json",
                                            "Access-Control-Allow-Origin": "*",
                                            "Keep-Alive": "timeout=25, max=1000"
                                          });
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                BorderRadius.circular(40)),
                                            elevation: 16,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(50),
                                                  color: Colors.lightGreen),
                                              child: ListView(
                                                shrinkWrap: true,
                                                children: <Widget>[
                                                  SizedBox(height: 20),
                                                  Center(
                                                      child: Text(
                                                          'Check your cart',
                                                          style: TextStyle(
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold))),
                                                  SizedBox(height: 20),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    }on Exception{

                                    }

                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])),
                    // Card(
                    //     margin: EdgeInsets.all(0.0),
                    //     child: Column(
                    //         mainAxisSize: MainAxisSize.min,
                    //         children: [
                    //           ListTile(
                    //             contentPadding: EdgeInsets.all(10.0),
                    //             leading: Image.asset('assets/images/Salle.jpg'),
                    //             title: Text("Band N° 3" ,
                    //                 style: TextStyle(
                    //                     fontSize: 15,
                    //                     fontWeight: FontWeight.bold
                    //                 )
                    //             ),
                    //           ),
                    //           ButtonBar(
                    //             alignment: MainAxisAlignment.spaceAround,
                    //             children: [
                    //               TextButton(
                    //                   onPressed: () {
                    //                     Navigator.pushNamed(context, 'HomePage');
                    //                   },
                    //                   child:Text(
                    //                     'Explore Plans',
                    //                     style: TextStyle(
                    //                         fontSize: 20.0,
                    //                         fontWeight: FontWeight.normal
                    //                     ),)
                    //               ),
                    //             ],
                    //           ),
                    //         ]     )
                    //
                    // ) Card 2
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        nameOfItems[1],
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Divider(
                      height: 10,
                      indent: 15.0,
                      endIndent: 15.0,
                      thickness: 3.5,
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                            Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset("assets/images/f2.png"),
                            title: Text(descriptionofItems[1],
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () async {
                                    // Navigator.pushNamed(context, 'CartPage');

                                    //
                                    try {
                                      int idcommande = 7;
                                      final response = await http.get(
                                          Uri.parse(
                                              'http://10.0.2.2:5000/addCommand?id=6&idcommande=' +
                                                  idcommande.toString()),
                                          headers: {
                                            "Accept": "application/json",
                                            "Access-Control-Allow-Origin": "*",
                                            "Keep-Alive": "timeout=25, max=1000"
                                          });

                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                BorderRadius.circular(40)),
                                            elevation: 16,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(50),
                                                  color: Colors.lightGreen),
                                              child: ListView(
                                                shrinkWrap: true,
                                                children: <Widget>[
                                                  SizedBox(height: 20),
                                                  Center(
                                                      child: Text(
                                                          'Check your cart',
                                                          style: TextStyle(
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold))),
                                                  SizedBox(height: 20),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    }on Exception{

                                                }
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                            Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/f2.png'),
                            title: Text("Band N° 3",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () async {
                                    // Navigator.pushNamed(context, 'CartPage');

                                    //
                                    int idcommande = 4;
                                    final response = await http.get(
                                        Uri.parse(
                                            'http://10.0.2.2:5000/addCommand?id=6&idcommande=' +
                                                idcommande.toString()),
                                        headers: {
                                          "Accept": "application/json",
                                          "Access-Control-Allow-Origin": "*"
                                        });
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ]))
                  ],
                ),
              ),
            ),
            TextButton.icon(
                onPressed: () {
                 // Navigator.pushNamed(context, 'AddOffer');
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => AddOffer() ));

                },
                icon: Icon(Icons.add),
                label: Text(
                  'Add a new offer',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                ))
          ],
        ),
      ),
    );
  }
}
