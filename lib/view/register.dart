
import 'dart:convert';

import 'package:eventiii/view/sign_in.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import '../viewmodel/User.dart';
import 'package:http/http.dart' as http;

import 'home.dart';
import '../main.dart';


class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {


  String toOriginalFormatString(DateTime dateTime) {
    final y = dateTime.year.toString().padRight(4, '0');
    final m = dateTime.month.toString().padRight(2, '0');
    final d = dateTime.day.toString().padRight(2, '0');
    return "$d-$m-$y";
  }



  bool isValidDate(String input) {

    try {


      final date = DateTime.parse(input);
      return true;
        }
    on Exception {

    return false;
    }

  }


  late String password="";
  late String username="";
  late String firstname="";
  late String lastname="";
  late String address="";
  late String nom="";
  late String datee="";
  late String errormsgg="";
  late Future<User> user;






  Future<User>  register(String nom,String mail,String password,context) async {


    //http://localhost:5000/inscription?nom=sofiene&mail=medicc@gmail.com&password=medicc
    String url="http://10.0.2.2:5000/inscription?nom="+nom+"&mail="+mail+"&password="+password;






    final response = await http.get(Uri.parse(url),headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",

    });


    // If the server did return a 200 OK response,
    // then parse the JSON.
    //print(response.body);

    // print(await http.read(Uri.parse(url)));


    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      //print(jsonDecode(response.body));
      var res=jsonDecode(response.body);

      //int idSession=res['id'];
      //print(idSession);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> Eventi()));
      return User.fromJson(jsonDecode(response.body));

    }
    else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> SignIn()));

      throw Exception('Utilisateur non authentifié');
    }

  }




  @override
  Widget build(BuildContext context) {

    final _formKey = GlobalKey<FormState>();


    DateTime date;
    TextEditingController _dateController = TextEditingController();
    DateTime selectedDate = DateTime.now();

    bool _passwordVisible = false;

    final passwordValidator = MultiValidator([
      RequiredValidator(errorText: 'Password is required'),
      MinLengthValidator(8, errorText: 'Password must be at least 8 digits long'),
      PatternValidator(r'(?=.*?[#?!@$%^&*-])', errorText: 'Passwords must have at least one special character')
    ]);

    void validateAndSave() {

      if (_formKey.currentState!.validate()) {
        print('Form is valid');
      } else {
        print('Form is invalid');
      }
    }

    Future _selectDate(BuildContext context) async {
      final DateTime? picked = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(1960, 1),
          lastDate: DateTime(2100));
      if (picked != null && picked != selectedDate)
        setState(() {
          selectedDate = picked;
          var date =
              "${picked.toLocal().day}/${picked.toLocal().month}/${picked.toLocal().year}";
          _dateController.text = date;
        });
    }

    void initState() {
      _passwordVisible = false;
    }

    return Scaffold(
      body: SafeArea(
          child: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Color(0XFF75CFCF),
              ),
              const TopSginup(),
              Positioned(
                top: MediaQuery.of(context).size.height * 0.10,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.9,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Color(0xFFF8F9FA),
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(45),
                          topRight: Radius.circular(45))),
                    child: Form(
                      //key: _formKey,
                      child: ListView(
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Container(
                          //   height: 250,
                          //   width: MediaQuery.of(context).size.width * 0.8,
                          //   margin: EdgeInsets.only(
                          //       left: MediaQuery.of(context).size.width * 0.09),
                          //   child: Image.asset("assets/images/Salle.png"),
                          // ),
                          Padding(padding: EdgeInsets.all(20.0)),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: TextFormField(
                              //onTap: ,


                              keyboardType: TextInputType.text ,
                              onChanged : (value){
                                setState(() {
                                  firstname=value;
                                });
                              },
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(10.0),
                                labelText: 'First Name',


                                //errorText: 'Error message',
                                border: OutlineInputBorder(),
                                ),
                              ),
                            ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: TextFormField(

                              onChanged : (value){
                                setState(() {
                                  lastname=value;
                                });
                              },
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(10.0),
                                labelText: 'Last Name',

                                //errorText: 'Error message',
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: GestureDetector(
                              onTap: () {
                                _selectDate(context);
                                FocusScope.of(context).requestFocus(new FocusNode());},
                              child: TextFormField(

                                onChanged : (value){
                                  setState(() {
                                    datee=value;
                                  });
                                },
                                keyboardType: TextInputType.datetime,
                               /* validator: (value) {
                                  if (value!.isEmpty) {
                                    return "Please enter a date for your birth day";
                                  }
                                },
                                */

                                decoration: InputDecoration(
                                  icon: Icon(Icons.date_range),
                                  contentPadding: EdgeInsets.all(10.0),
                                  labelText: 'Birth day',
                                  helperText: 'Format : DD-MM-YYYY',
                                  //errorText: 'Error message',
                                  border: OutlineInputBorder(),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: TextFormField(

                                onChanged : (value){
                                  setState(() {
                                    username=value;
                                  });
                                },
                              validator: EmailValidator(errorText: 'your email is incorrect'),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(10.0),
                                  labelText: 'Email Address',
                                  //errorText: 'Error message',
                                  border: OutlineInputBorder(),
                                )
                            )
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: TextFormField(

                              onChanged : (value){
                                setState(() {
                                  address=value;
                                });
                              },
                              showCursor: true,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(10.0),
                                labelText: 'Address',
                                //errorText: 'Error message',
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: TextFormField(
                              obscureText: true,
                              onChanged: (val) => password = val,
                              validator: passwordValidator,
                              showCursor: true,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(10.0),
                                labelText: 'Password',
                                //errorText: 'Error message',
                                border: OutlineInputBorder(),
                                suffixIcon: IconButton(
                                    icon: Icon(Icons.remove_red_eye),
                                onPressed: () {
                                  _passwordVisible = !_passwordVisible;
                                },)
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: TextFormField(
                              obscureText: true,
                              onChanged: (val) => password = val,
                              validator: passwordValidator,
                              showCursor: true,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(10.0),
                                labelText: 'Confirm Password',
                                //errorText: 'Error message',
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child:Text(errormsgg,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red,
                                  fontSize: 10,
                                )
                            ),
                          ),



                          const CheckerBox(),
                          InkWell(


                            onTap: ()  async {
                              setState(() {
      //String nom,String mail,String password,

                                if(isValidDate(datee)){
                                  print("heeey");
                                  errormsgg="Format date : dd-mm-yy";

                                }
                                else if(username.length ==0 || password.length==0 || firstname.length==0 || lastname.length==0 ||  address.length==0 ) {
                                  print("formmm");

                                  errormsgg="Please fill up all the form";

                                            //aa
                                                                             }
                                else {
                                  nom = firstname + " " + lastname;
                                  user = register(
                                      nom, username, password, context);
                                    }
                              });
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height * 0.07,
                              margin: const EdgeInsets.only(left: 20, right: 20),
                              decoration: BoxDecoration(
                                  color: Color(0XFF75CFCF),
                                  borderRadius:
                                  const BorderRadius.all(Radius.circular(10))),
                              child: Center(
                                child: Text(
                                  "Sign up",
                                  style: TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xFFF8F9FA)),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all( 15.0),
                            child: Text.rich(
                              TextSpan(
                                  text: "I already have an account ",
                                  style: TextStyle(
                                      color: Color(0xFF9FA4AF).withOpacity(0.8), fontSize: 16),
                                  children: [
                                    TextSpan(
                                        text: "Sign In",
                                        style: TextStyle(color: Color(0XFF75CFCF), fontSize: 16),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            Navigator.pushReplacement(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) => SignIn()));
                                            print("Sign in click");
                                          }),
                                  ]),
                            ),
                          ),
                          Padding(padding: EdgeInsets.all(20.0))
                        ],
                      ),
                    ),
                  ),
                ),

            ]
          ),
      )
    );
  }
}

class CheckerBox extends StatefulWidget {
  const CheckerBox({
    Key? key,
  }) : super(key: key);

  @override
  State<CheckerBox> createState() => _CheckerBoxState();
}

class _CheckerBoxState extends State<CheckerBox> {
  bool? isCheck;
  @override
  void initState() {
    // TODO: implement initState
    isCheck = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(
              value: isCheck,
              checkColor: Color(0xFFF8F9FA), // color of tick Mark
              activeColor: Color(0XFF75CFCF),
              onChanged: (val) {
                setState(() {
                  isCheck = val!;
//                  print(isCheck);
                });
              }),
          Text.rich(
            TextSpan(
                text: "I agree with ",
                style:
                TextStyle(color: Color(0xFF9FA4AF).withOpacity(0.8), fontSize: 16),
                children: [
                  TextSpan(
                      text: "Terms ",
                      style: TextStyle(color: Color(0XFF75CFCF), fontSize: 16)),
                  const TextSpan(text: "and "),
                  TextSpan(
                      text: "Policy",
                      style: TextStyle(color: Color(0XFF75CFCF), fontSize: 16)),
                ]),
          ),
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class InputField extends StatelessWidget {
  String headerText;
  String hintTexti;
  InputField({Key? key, required this.headerText, required this.hintTexti})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(
            left: 20,
            right: 20,
            bottom: 10,
          ),
          child: Text(
            headerText,
            style: const TextStyle(
                color: Colors.black, fontSize: 22, fontWeight: FontWeight.w500),
          ),
        ),
        Container(
            margin: const EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
              color: Color(0xFF9FA4AF).withOpacity(0.5),
              // border: Border.all(
              //   width: 1,
              // ),
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: TextField(
                decoration: InputDecoration(
                  hintText: hintTexti,
                  border: InputBorder.none,
                ),
              ),
            )
          //IntrinsicHeight

        ),
      ],
    );
  }
}

class InputFieldPassword extends StatefulWidget {
  String headerText;
  String hintTexti;

  InputFieldPassword(
      {Key? key, required this.headerText, required this.hintTexti})
      : super(key: key);

  @override
  State<InputFieldPassword> createState() => _InputFieldPasswordState();
}

class _InputFieldPasswordState extends State<InputFieldPassword> {
  bool _visible = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(
            left: 20,
            right: 20,
            bottom: 10,
          ),
          child: Text(
            widget.headerText,
            style: const TextStyle(
                color: Colors.black, fontSize: 22, fontWeight: FontWeight.w500),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 20, right: 20),
          decoration: BoxDecoration(
            color: Color(0xFF9FA4AF).withOpacity(0.5),
            // border: Border.all(
            //   width: 1,
            // ),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: TextField(
              obscureText: _visible,
              decoration: InputDecoration(
                  hintText: widget.hintTexti,
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                      icon: Icon(
                          _visible ? Icons.visibility : Icons.visibility_off),
                      onPressed: () {
                        setState(() {
                          _visible = !_visible;
                        });
                      })),
            ),
          ),
        ),
      ],
    );
  }
}

class TopSginup extends StatelessWidget {
  const TopSginup({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AppBar(
        backgroundColor: Color(0XFF75CFCF),
        title: Text('Create your account',
          style: TextStyle(
            fontWeight: FontWeight.w900,
            fontSize: 25,
            letterSpacing: 0.5
          ),

        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}

