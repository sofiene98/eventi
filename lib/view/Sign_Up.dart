import 'package:flutter/material.dart';
import 'config.dart';
import 'constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:video_player/video_player.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  // late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    // _controller = VideoPlayerController.asset(UIGuide.backgroundvid)
    //   ..initialize().then((_) {
    //     _controller.play();
    //     _controller.setLooping(true);
    //     setState(() {});
    //   });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Stack(
          children: <Widget>[
            // SizedBox.expand(
            //   child: FittedBox(
            //     fit: BoxFit.fitHeight,
            //     child: Container(
            //       color: Color(0XFF075CFCF),
            //       width: _controller.value.size.width,
            //       height: _controller.value.size.height,
            //     ),
            //   ),
            // ),
            _skip(context),
            SizedBox(),
            _logo(),
            SizedBox(
              height: 50,
            ),
            Positioned(
                child: Container(child: Center(child: tabWidget(context))))
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    //_controller.dispose();
  }
}

Widget tabWidget(context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            "We are happy to see you here!",
            style: GoogleFonts.poppins(
              fontSize: 20,
              color: Colors.teal,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            height: 30,
            child: DefaultTextStyle(
              style: GoogleFonts.montserrat(
                fontSize: 30,
                color: Colors.white,
                fontWeight: FontWeight.w500,
              ),
              child: AnimatedTextKit(
                animatedTexts: [
                  TyperAnimatedText('Welcome'),
                  TyperAnimatedText('Bienvenue'),
                  TyperAnimatedText('Willkommen'),
                ],
                repeatForever: true,
                pause: const Duration(milliseconds: 1200),
                stopPauseOnTap: true,
              ),
            ),
          ),
        ],
      ),
      SizedBox(height: 20),
      Container(
        width: 300,
        height: 50,
        // ignore: deprecated_member_use
        child: RaisedButton(
          onPressed: () => {},
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                'assets/images/google.png',
                // height: SizeConfig.blockSizeVertical * 2.5,
                // width: SizeConfig.blockSizeHorizontal * 5,
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  "Sign up with Google",
                  style: GoogleFonts.poppins(
                      fontSize: 15,
                      color: Colors.orange,
                      letterSpacing: 0.168,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
          color: Colors.black.withOpacity(0.5),
        ),
      ),
      SizedBox(height: 20),
      Container(
        width: 300,
        height: 50,
        // ignore: deprecated_member_use
        child: RaisedButton(
          onPressed: () => {},
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: [
              FaIcon(
                FontAwesomeIcons.facebook,
                color: Colors.white,
              ),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: Text(
                  "Sign up with FaceBook",
                  style: GoogleFonts.poppins(
                      fontSize: 15,
                      color: Colors.orange,
                      letterSpacing: 0.168,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
          color: Colors.black.withOpacity(0.5),
        ),
      ),
      SizedBox(height: 20),
      Container(
        width: 300,
        height: 50,
        // ignore: deprecated_member_use
        child: RaisedButton(
          onPressed: () => {Navigator.pushNamed(context, 'Register')},
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: [
              const FaIcon(
                Icons.email,
                color: Colors.white,
              ),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: Text(
                  "Sign up with email",
                  style: GoogleFonts.poppins(
                      fontSize: 15,
                      color: Colors.orange,
                      letterSpacing: 0.168,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
          color: Colors.black.withOpacity(0.5),
        ),
      ),
      SizedBox(height: 15),
    ],
  );
}

// Widget _already() {
//   return Row(
//     mainAxisAlignment: MainAxisAlignment.center,
//     children: [
//       Text(
//         "I Already have an account  ",
//         style: GoogleFonts.poppins(fontSize: 12, color: Colors.white),
//       ),
//       InkWell(
//         child: Text(
//           "Sign In",
//           style: GoogleFonts.poppins(
//               fontSize: 12,
//               fontWeight: FontWeight.bold,
//               decoration: TextDecoration.underline,
//               color: Colors.white),
//         ),
//         onTap: () {},
//       )
//     ],
//   );
// }

Widget _logo() {
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 50),
    child: Column(
      children: [
        const Padding(padding: EdgeInsets.only()),
        Center(
            child: Text(
          'Eventi',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
        )),
      ],
    ),
  );
}

Widget _skip(context) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 40),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        InkWell(
          child: Text(
            "Skip",
            style: GoogleFonts.poppins(
                fontSize: 15, color: Colors.white, fontWeight: FontWeight.w500),
          ),
          onTap: () {
            Navigator.pushNamed(context, 'HomePage');
          },
        )
      ],
    ),
  );
}
