import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import '../main.dart';
import 'package:form_field_validator/form_field_validator.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Edit Profile',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold
          ),
        ),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back));
          },
        ),
        backgroundColor: Color(0XFF75CFCF),
      ),
      bottomNavigationBar: BottomNavigationBar(
        //mouseCursor: SystemMouseCursor._(kind: 'click') ,
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        backgroundColor: Color(0XFF75CFCF),
        selectedItemColor: Color(0xFFFFFBFA),
        unselectedItemColor: Color(0xFFFFFBFA).withOpacity(0.60) ,
        onTap: (value) {
          setState(() {
            _currentIndex=value;
            print(_currentIndex);
            //screens[_currentIndex];

          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.format_list_bulleted),
              label: 'Categories'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.games),
              label: 'Games'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart),
              label: 'Cart'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            label: 'Profile',
          ),
        ],
      ),
      body: SafeArea(
        child: Stack(
          children: [
            ListView(
              children: [

              ],
            ),
            Column(
              children: [
                Container(
                  height: 200,
                  color: Color(0XFF75CFCF),
                  child: Container(
                      width: double.infinity,
                      height: 80,
                      child: Container(
                        alignment: Alignment(0.0,10.0),
                        child: CircleAvatar(
                          radius: 90.0,
                          backgroundColor: Colors.white,
                          child: CircleAvatar(
                            backgroundImage: AssetImage('assets/images/Mahdi Hadj Kacem.jpg'),
                            radius: 80.0,
                          ),

                        ),
                      )
                  ),
                ),
                SizedBox(height: 100),
                Text(
                  'Mahdi Hadj Kacem',
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                  ),

                ),
                SizedBox(
                  height: 50,
                )
              ],
            ),
            Stack(
              children: [
                Container(
                  child: Positioned(
                    top: MediaQuery.of(context).size.height * 0.40,
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.9,
                      width: MediaQuery.of(context).size.width,

                      child: Form(
                        //key: _formKey,
                        child: ListView(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Container(
                            //   height: 250,
                            //   width: MediaQuery.of(context).size.width * 0.8,
                            //   margin: EdgeInsets.only(
                            //       left: MediaQuery.of(context).size.width * 0.09),
                            //   child: Image.asset("assets/images/Salle.png"),
                            // ),
                            Padding(padding: EdgeInsets.all(20.0)),
                            Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: TextFormField(
                                //onTap: ,
                                keyboardType: TextInputType.text ,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(10.0),
                                  labelText: 'First Name',
                                  //errorText: 'Error message',
                                  border: OutlineInputBorder(),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: TextFormField(
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(10.0),
                                  labelText: 'Last Name',
                                  //errorText: 'Error message',
                                  border: OutlineInputBorder(),
                                ),
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: TextFormField(
                                    validator: EmailValidator(errorText: 'your email is incorrect'),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(10.0),
                                      labelText: 'Email Address',
                                      //errorText: 'Error message',
                                      border: OutlineInputBorder(),
                                    )
                                )
                            ),
                            InkWell(

                              onTap: () {
                                //validateAndSave();
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height * 0.07,
                                margin: const EdgeInsets.only(left: 20, right: 20),
                                decoration: BoxDecoration(
                                    color: Color(0XFF75CFCF),
                                    borderRadius:
                                    const BorderRadius.all(Radius.circular(10))),
                                child: Center(
                                  child: Text(
                                    "Save changes",
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xFFF8F9FA)),
                                  ),
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.all(20.0))
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            // Container(
            //   height: 200,
            //   color: Color(0XFF75CFCF),
            //   child: Container(
            //       width: double.infinity,
            //       height: 80,
            //       child: Container(
            //         alignment: Alignment(0.0,10.0),
            //         child: CircleAvatar(
            //           radius: 90.0,
            //           backgroundColor: Colors.white,
            //           child: CircleAvatar(
            //             backgroundImage: AssetImage('assets/images/Mahdi Hadj Kacem.jpg'),
            //             radius: 80.0,
            //           ),
            //
            //         ),
            //       )
            //   ),
            // ),
            // Text(
            //   'Mahdi Hadj Kacem',
            //   style: TextStyle(
            //       fontSize: 20,
            //       fontWeight: FontWeight.bold
            //   ),
            //
            // ),
            // SingleChildScrollView(
            //
            //   //top: MediaQuery.of(context).size.height * 0.10,
            //     child: Form(
            //
            //       //key: _formKey,
            //       child: ListView(
            //         //crossAxisAlignment: CrossAxisAlignment.start,
            //         children: [
            //           Padding(padding: EdgeInsets.all(20.0)),
            //           Padding(
            //             padding: const EdgeInsets.all(15.0),
            //             child: TextFormField(
            //               //onTap: ,
            //               keyboardType: TextInputType.text ,
            //               decoration: InputDecoration(
            //                 contentPadding: EdgeInsets.all(10.0),
            //                 labelText: 'First Name',
            //                 //errorText: 'Error message',
            //                 border: OutlineInputBorder(),
            //               ),
            //             ),
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(15.0),
            //             child: TextFormField(
            //               decoration: InputDecoration(
            //                 contentPadding: EdgeInsets.all(10.0),
            //                 labelText: 'Last Name',
            //                 //errorText: 'Error message',
            //                 border: OutlineInputBorder(),
            //               ),
            //             ),
            //           ),
            //           Padding(
            //               padding: const EdgeInsets.all(15.0),
            //               child: TextFormField(
            //                   //validator: EmailValidator(errorText: 'your email is incorrect'),
            //                   decoration: InputDecoration(
            //                     contentPadding: EdgeInsets.all(10.0),
            //                     labelText: 'Email Address',
            //                     //errorText: 'Error message',
            //                     border: OutlineInputBorder(),
            //                   )
            //               )
            //           )
            //         ],
            //       ),
            //     ),
            // ),

          ],
        ),
      ),
    );
  }
}
