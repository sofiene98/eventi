import 'package:eventiii/view/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/typicons_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../main.dart';
import '';

class Games extends StatefulWidget {
  const Games({Key? key}) : super(key: key);

  @override
  _GamesState createState() => _GamesState();
}

class _GamesState extends State<Games> {
  int _currentIndex = 0;


  // void initState() {
  //   super.initState();
  // }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      //   bottomNavigationBar: BottomNavigationBar(
      //     //mouseCursor: SystemMouseCursor._(kind: 'click') ,
      //     type: BottomNavigationBarType.fixed,
      //     currentIndex: _currentIndex,
      //     backgroundColor: Color(0XFF75CFCF),
      //     selectedItemColor: Color(0xFFFFFBFA),
      //     unselectedItemColor: Color(0xFFFFFBFA).withOpacity(0.60) ,
      //     onTap: (value) {
      //       setState(() {
      //         _currentIndex=value;
      //         print(_currentIndex);
      //         //screens[_currentIndex];
      //
      //       });
      //     },
      //     items: [
      //       BottomNavigationBarItem(
      //           icon: Icon(Icons.home),
      //           label: 'Home'
      //       ),
      //       BottomNavigationBarItem(
      //           icon: Icon(Icons.format_list_bulleted),
      //           label: 'Categories'
      //       ),
      //       BottomNavigationBarItem(
      //           icon: Icon(Icons.games),
      //           label: 'Games'
      //       ),
      //       BottomNavigationBarItem(
      //           icon: Icon(Icons.shopping_cart),
      //           label: 'Cart'
      //       ),
      //       BottomNavigationBarItem(
      //         icon: Icon(Icons.person_outline),
      //         label: 'Profile',
      //       ),
      //     ],
      //   ),
      // appBar: AppBar(
      //   title: Text('Games',
      //     style: TextStyle(
      //       fontSize: 20,
      //       fontWeight: FontWeight.bold
      //     ),
      //   ),
      //   leading: Builder(
      //     builder: (BuildContext context) {
      //       return IconButton(
      //           onPressed: () {
      //             Navigator.pop(context);
      //           },
      //           icon: Icon(Icons.arrow_back));
      //     },
      //   ),
      //   backgroundColor: Color(0XFF75CFCF),
      // ),
      body: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: FittedBox(
              fit: BoxFit.fitHeight,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("Eventi",
                            style: TextStyle(
                                fontSize: 40,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                        Text("1260", style: TextStyle(fontFamily: 'Montserrat', fontSize: 40, fontWeight: FontWeight.bold),
                        ),
                        IconButton(
                          onPressed: (){Navigator.pushNamed(context, 'LandingScreen');
                          },
                          icon: Icon(FontAwesomeIcons.coins),color: Color(0XFFFFE589),),
                  ],


      ),
                    SizedBox(
                      height : 100,
                      width: MediaQuery. of(context). size. width * 0.95,
                      child: DecoratedBox(
                        child: Center(
                          child: Text(
                            'Gagnez 10 points',
                            textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                          ),),
                        ),
                        decoration: BoxDecoration(
                          color: Color(0XFF75CFCF),
                          borderRadius: BorderRadius.circular(10),

                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children : <Widget>[
                        Text('Turbo Eventi', style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                        )),
                        IconButton(onPressed: () {}, icon: const Icon(FontAwesomeIcons.info, color: Colors.black,))
                      ]
                    ),
                    SizedBox(
                      height : 100,
                      width: MediaQuery. of(context). size. width * 0.95,
                      child: DecoratedBox(
                        child: Center(
                          child: Text(
                            'Gagnez 100 points',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 40,
                            ),),
                        ),
                        decoration: BoxDecoration(
                          color: Color(0XFFFFE589),
                          borderRadius: BorderRadius.circular(10),

                        ),
                      ),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children : <Widget>[
                          Text('Turbo Eventi', style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                          )),
                          IconButton(onPressed: () {}, icon: const Icon(FontAwesomeIcons.info, color: Colors.black,))
                        ]
                    ),
                    SizedBox(
                      height : 100,
                      width: MediaQuery. of(context). size. width * 0.95,
                      child: DecoratedBox(
                        child: Center(
                          child: Text(
                            'Dernier Gagnant Super',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 40,
                            ),),
                        ),
                        decoration: BoxDecoration(
                          color: Color(0XFFFFC3E7),
                          borderRadius: BorderRadius.circular(10),

                        ),
                      ),
                    ),

    ]),
              ),
            ),
          ),
    )
    );
  }
}



