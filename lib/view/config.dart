import 'package:flutter/material.dart';

// app configurations
class AppConfig {
  static const appName = "Flutter Login Templates";
}

//Class to get Screen Size
class SizeConfig {
  static const MediaQueryData _mediaQueryData = MediaQueryData();
  static double screenWidth =_mediaQueryData.size.width;
  static double screenHeight=_mediaQueryData.size.height;
  static double blockSizeHorizontal=screenWidth / 100;
  static double blockSizeVertical=screenHeight / 100;

  static final double _safeAreaHorizontal=_mediaQueryData.padding.left + _mediaQueryData.padding.right;
  static final double _safeAreaVertical=_mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
  static double safeBlockHorizontal=(screenWidth - _safeAreaHorizontal) / 100;
  static double safeBlockVertical=(screenHeight - _safeAreaVertical) / 100;

  // void init(BuildContext context) {
  //   _mediaQueryData = MediaQuery.of(context);
  //   screenWidth = _mediaQueryData.size.width;
  //   screenHeight = _mediaQueryData.size.height;
  //   blockSizeHorizontal = screenWidth / 100;
  //   blockSizeVertical = screenHeight / 100;
  //
  //   _safeAreaHorizontal =
  //       _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  //   _safeAreaVertical =
  //       _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
  //   safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
  //   safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
  // }
}