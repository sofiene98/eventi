import 'package:flutter/material.dart';
class Band extends StatefulWidget {
  const Band({Key? key}) : super(key: key);

  @override
  _BandState createState() => _BandState();
}

class _BandState extends State<Band> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        'First Typee ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    //First Type
                    Divider(
                      height: 10,
                      indent: 15.0,
                      endIndent: 15.0,
                      thickness: 3.5,
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                        Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/Band 3.jpg'),
                            title: Text("Band Bodouro",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, 'HomePage');
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Second Type ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Divider(
                      height: 10,
                      indent: 15.0,
                      endIndent: 15.0,
                      thickness: 3.5,
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                        Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/Band 4.jpg'),
                            title: Text("Orchestre Tunisien",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, 'HomePage');
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                        Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/Band 5.jpg'),
                            title: Text("Bassem Frères",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, 'HomePage');
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ]))
                  ],
                ),
              ),
            ),
            TextButton.icon(
                onPressed: () {

                  Navigator.pushNamed(context, 'AddOffer');
                },
                icon: Icon(Icons.add),
                label: Text(
                  'Add a new offer',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                ))
          ],
        ),

      ),
    );
  }
}
