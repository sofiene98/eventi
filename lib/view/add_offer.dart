//import 'dart:html';
import 'package:dropdown_search/dropdown_search.dart';
import 'sign_in.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:camera/camera.dart';
import 'package:http/http.dart' as http;

class AddOffer extends StatefulWidget {
  const AddOffer({Key? key}) : super(key: key);

  @override
  _AddOfferState createState() => _AddOfferState();
}

class _AddOfferState extends State<AddOffer> {

// Attributes
  XFile? _image;
  late String nom_annonce = "Tunisian Food";
  late String dateA = "";
  late String location = "";
  late String description = "This is a";
  late double duration = -1;
  late String categorie = "Food";
  late double price = -1;
  late double discount = -1;

  late String errormsg = "";
  late bool formulaire = true;

  //final file = ImageFile.toFile();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          'Add a new offer',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back));
          },
        ),
        backgroundColor: Color(0XFF75CFCF),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 5.0,
                primary: Colors.blueAccent.withOpacity(0.6),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
              ),
              //When we click on Add Offer
              onPressed: () async {
                //Navigator.pushNamed(context, 'Multiple choice');
                      print("Button clicked");
                try {

                  print(nom_annonce+location+description+duration.toString()+categorie+price.toString());
                  if (nom_annonce.length > 0 &&
                      dateA.length > 0 &&
                      location.length > 0 &&
                      description.length > 0 &&
                      duration != -1 &&
                      categorie.length > 0 &&
                      price != -1 &&
                      discount != -1) {
                    //http://localhost:5000/insertAnnonce?nom_annonce=aaaa&dateA=aaaa&location=aaaa&description=aaa&duration=4&categorie=eeee&prix=4&discount=7
                    String ins = "nom_annonce=" +
                        nom_annonce +
                        "&dateA=" +
                        dateA +
                        "&location=" +
                        location +
                        "&description=" +
                        description +
                        "&duration=" +
                        duration.toString() +
                        "&categorie=" +
                        categorie +
                        "&prix=" +
                        price.toString() +
                        "&discount=" +
                        discount.toString();

                    // Insert Annonce to the database
                    String url = "http://10.0.2.2:5000/insertAnnonce?" + ins;
                    print(url);
                    final response = await http.get(Uri.parse(url), headers: {
                      "Accept": "application/json",
                      "Access-Control-Allow-Origin": "*",
                    });

                    showDialog(
                      context: context,
                      builder: (context) {
                        return Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40)),
                          elevation: 16,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.lightGreen),
                            child: ListView(
                              shrinkWrap: true,
                              children: <Widget>[
                                SizedBox(height: 20),
                                Center(
                                    child: Text('Offre added',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold))),
                                SizedBox(height: 20),
                              ],
                            ),
                          ),
                        );
                      },
                    );


                  }

                  else {
                    errormsg="Erreur formulaire";
                    print(errormsg);
                  }
                } on Exception {

                }
              },
              child: Text(
                'Add Offer',
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
      body: Stack(children: [
        Positioned(
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Color(0xFFF8F9FA),
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(45),
                    topRight: Radius.circular(45))),
            child: Form(
              //key: _formKey,
              child: ListView(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Container(
                  //   height: 250,
                  //   width: MediaQuery.of(context).size.width * 0.8,
                  //   margin: EdgeInsets.only(
                  //       left: MediaQuery.of(context).size.width * 0.09),
                  //   child: Image.asset("assets/images/Salle.png"),
                  // ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: GestureDetector(
                      onTap: () {
                        //_selectDate(context);
                        FocusScope.of(context).requestFocus(new FocusNode());
                      },
                      child: TextFormField(
                        //controller: _dateController,
                        keyboardType: TextInputType.datetime,

                        onChanged: (value) {
                          setState(() {
                            dateA = value;
                            print(dateA);
                            //print(username);
                          });
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Please enter a date for your offer";
                          }
                        },
                        decoration: InputDecoration(
                          icon: Icon(Icons.date_range),
                          contentPadding: EdgeInsets.all(10.0),
                          labelText: 'Date',
                          helperText: 'Format : DD-MM-YYYY',
                          //errorText: 'Error message',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: TextFormField(
                          //validator: EmailValidator(errorText: 'your email is incorrect'),

                          onChanged: (value) {
                            setState(() {
                              location = value;
                              //print(username);
                            });
                          },
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(10.0),
                            labelText: 'Location',
                            helperText: 'Please give a precise address',
                            //errorText: 'Error message',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ))),

                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: TextFormField(
                      //onTap: ,
                      keyboardType: TextInputType.text,
                      onChanged: (value) {
                        setState(() {
                          try {
                            duration = double.parse(value);
                          } on Exception {
                            formulaire = false;
                            errormsg = "Duration must be double";
                          }
                          //print(username);
                        });
                      },
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(10.0),
                        labelText: 'Duration',
                        helperText: 'Format D Days - H Hours',
                        //errorText: 'Error message',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: DropdownSearch<String>(
                      validator: (v) => v == null ? "required field" : null,
                      dropdownSearchDecoration: InputDecoration(
                        hintText: "Select a country",
                        labelText: "Menu mode with helper *",
                        helperText: 'positionCallback example usage',
                        contentPadding: EdgeInsets.fromLTRB(12, 12, 0, 0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      mode: Mode.MENU,
                      showSelectedItems: true,
                      items: [
                        "Private Venue",
                        "Food",
                        "Band",
                        'Hosting Services',
                        'Other Services'
                      ],
                      onChanged: print,
                      selectedItem: "Private Venue",
                      positionCallback: (popupButtonObject, overlay) {
                        final decorationBox = _findBorderBox(popupButtonObject);

                        double translateOffset = 0;
                        if (decorationBox != null) {
                          translateOffset = decorationBox.size.height -
                              popupButtonObject.size.height;
                        }

                        // Get the render object of the overlay used in `Navigator` / `MaterialApp`, i.e. screen size reference
                        final RenderBox overlay = Overlay.of(context)!
                            .context
                            .findRenderObject() as RenderBox;
                        // Calculate the show-up area for the dropdown using button's size & position based on the `overlay` used as the coordinate space.
                        return RelativeRect.fromSize(
                          Rect.fromPoints(
                            popupButtonObject
                                .localToGlobal(
                                    popupButtonObject.size
                                        .bottomLeft(Offset.zero),
                                    ancestor: overlay)
                                .translate(0, translateOffset),
                            popupButtonObject.localToGlobal(
                                popupButtonObject.size.bottomRight(Offset.zero),
                                ancestor: overlay),
                          ),
                          Size(overlay.size.width, overlay.size.height),
                        );
                      },
                    ),
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: TextFormField(
                            //onTap: ,

                            onChanged: (value) {
                              setState(() {
                                try {
                                  price = double.parse(value);
                                } on Exception {
                                  formulaire = false;
                                  errormsg = "Price must be double";
                                }
                                //print(username);
                              });
                            },
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(10.0),
                              labelText: 'Price TND',
                              //errorText: 'Error message',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: TextFormField(
                            //onTap: ,
                            onChanged: (value) {
                              setState(() {
                                try {
                                  discount = double.parse(value);
                                } on Exception {
                                  formulaire = false;
                                  errormsg = "Discount must be double";
                                }
                                //print(username);
                              });
                            },
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(10.0),
                              labelText: 'Discount %',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  //const CheckerBox(),
                  SizedBox(
                    height: 20.0,
                  )
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }
}

RenderBox? _findBorderBox(RenderBox box) {
  RenderBox? borderBox;

  box.visitChildren((child) {
    if (child is RenderCustomPaint) {
      borderBox = child;
    }

    final box = _findBorderBox(child as RenderBox);
    if (box != null) {
      borderBox = box;
    }
  });

  return borderBox;
}

void _showPicker(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Photo Library'),
                    onTap: () {
                      //_imgFromGallery();
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text('Camera'),
                  onTap: () {
                    getImagefromCamera();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      });
}

Future getImagefromCamera() async {
  final ImagePicker _picker = ImagePicker();

  final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
}



