import 'dart:convert';
import 'dart:typed_data';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:eventiii/view/config.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:slide_countdown/slide_countdown.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:http/http.dart' as http;

import 'package:eventiii/viewmodel/Album.dart';
import 'package:eventiii/viewmodel/note.dart';
import 'package:image/image.dart' as ImageProcess;

import 'dart:typed_data';

Future<Album> fetchAlbum() async {
  final response = await http
      .get(Uri.parse('https://jsonplaceholder.typicode.com/albums/1'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return Album.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

class Home extends StatefulWidget {
  final idd;

  const Home({Key? key, this.idd}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
//  List<Note> _notes = <Note>[];
  late Future<Album> futureAlbum;

  late Future<Widget> imagee;
  late String _base64 = "";

  late List<String> nameOfItems = [];
  late List<String> descriptionofItems = [];
  late List<String> listDate = [];

  //final _byteImage = Base64Decoder().convert(base64Image);
  /*Widget image = Image.memory(_byteImage);
  //print("heeeey");
  print(base64Image);
  return image;

   */
  List<String> listofItems = [];

  @override
  void initState() {
    super.initState();
    //futureAlbum = fetchAlbum();

    this.listofItems = [
      "assets/images/S4.png",
      "assets/images/Salle.jpg",
      "assets/images/f1.png"
    ]; // b64
  }

  int _current = 0;
  int _currentIndex = 0;

  final controller = PageController(viewportFraction: 0.8, keepPage: true);
  final CarouselController _controller = CarouselController();
  ScrollController _rrectController = ScrollController();

  @override
  Widget build(BuildContext context) {
    //Uint8List bytes = Base64Decoder().convert(_base64);

    this.listofItems = [
      "assets/images/S4.png",
      "assets/images/Salle.jpg",
      "assets/images/f1.png"
    ]; // b64

    return Scaffold(
      body: SingleChildScrollView(
        //Use list here in the body section of the Scaffold

        child: Column(
          children: <Widget>[
            // new ListTile(
            //   leading: new Image.memory(bytes),
            //
            // ),

            /* FutureBuilder<Album>(
              future: futureAlbum,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Text(snapshot.data!.title);
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }

                // By default, show a loading spinner.
                return const CircularProgressIndicator();
              },
            ),

            */

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "Eventi", // Text ("${widget.idd}")
                    style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                  ),
                ),
                Text(
                  "1260",
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                IconButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'LandingScreen');
                    },
                    icon: Icon(Icons.money, color: Color(0XFFffc300)),
                    iconSize: 50)
              ],
            ),
            SizedBox(height: 32),
            Column(
              children: [
                CarouselSlider(
                  carouselController: _controller,
                  options: CarouselOptions(
                      height: 150.0,
                      scrollDirection: Axis.horizontal,
                      autoPlay: true,
                      enlargeCenterPage: true,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _current = index;
                        });
                      }),
                  items: [1, 2, 3].map((index) {
                    // length list
                    //  Uint8List bytess = Base64Decoder().convert(_base64);
                    return Builder(
                      builder: (BuildContext context) {
                        return Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.symmetric(horizontal: 20.0),
                            child: Container(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Image.asset(listofItems[index - 1],
                                      fit: BoxFit.cover),
                                )));
                      },
                    );
                  }).toList(),
                ),
                //  SizedBox(height: 32),
                AnimatedSmoothIndicator(
                  activeIndex: _current,
                  count: this.listofItems.length,
                  effect: WormEffect(
                      dotHeight: 10,
                      dotWidth: 10,
                      activeDotColor: Colors.black,
                      dotColor: Colors.grey),
                ),
              ],
            ),
            // SizedBox(height: 20),
            Container(
              color: Color(0XFFFFC300),
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text("Special Offers",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                          letterSpacing: 0.5)),
                  SlideCountdownSeparated(
                    duration: Duration(hours: 2, minutes: 3, seconds: 59),
                    fade: true,
                    separatorType: SeparatorType.symbol,
                    durationTitle: DurationTitle.id(),
                    icon: const Padding(
                      padding: EdgeInsets.only(right: 5),
                      child: Icon(
                        Icons.alarm,
                        color: Colors.white,
                        size: 20,
                      ),
                    ),
                    decoration: const BoxDecoration(
                      color: Colors.amber,
                    ),
                  )
                ],
              ),
            ),
            Card(
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: Image.asset("assets/images/S4.png"),
                    subtitle: Text("Top Happiness"),
                    //Text("Dates : Jan 2022 -> Mar 2022")
                    title: Text("Band",
                        style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Nice experience"),
                  ),
                  ButtonBar(
                    alignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: Icon(FontAwesome.heart),
                        color: Colors.black,
                        onPressed: () async {
                          try {
                            int idcommande = 1;
                            final response = await http.get(
                                Uri.parse('http://10.0.2.2:5000/insertOfs?ofs=' +
                                    idcommande.toString()),
                                headers: {
                                  "Accept": "application/json",
                                  "Access-Control-Allow-Origin": "*",
                                  "Keep-Alive": "timeout=25, max=1000"
                                });

                            showDialog(
                              context: context,
                              builder: (context) {
                                return Dialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(40)),
                                  elevation: 16,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: Colors.lightGreen),
                                    child: ListView(
                                      shrinkWrap: true,
                                      children: <Widget>[
                                        SizedBox(height: 20),
                                        Center(
                                            child: Text('Check your saved Items',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold))),
                                        SizedBox(height: 20),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          } on Exception {}
                        },
                      ),
                      IconButton(
                        alignment: Alignment.topLeft,
                        icon: Icon(Icons.share),
                        onPressed: () {
                          Navigator.pushNamed(context, 'Games');
                        },
                      ),
                      FlatButton(
                          onPressed: () {
                            Navigator.pushNamed(context, 'Profile');
                          },
                          textTheme: ButtonTextTheme.primary,
                          child: Text(
                            'Check it out !',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          )),
                    ],
                  ),
                ])),
            Card(
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: Image.asset("assets/images/Salle.jpg"),
                    subtitle: Text("Sophie Wedding Venue"),
                    title: Text("Smooth Wedding",
                        style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Greyhound divisively hello coldly wonderfully marginally far upon excluding'),
                  ),
                  ButtonBar(
                    alignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: Icon(FontAwesome.heart),
                        color: Colors.black,
                        onPressed: () async {
                          try {
                            int idcommande = 2;
                            final response = await http.get(
                                Uri.parse('http://10.0.2.2:5000/insertOfs?ofs=' +
                                    idcommande.toString()),
                                headers: {
                                  "Accept": "application/json",
                                  "Access-Control-Allow-Origin": "*",
                                  "Keep-Alive": "timeout=25, max=1000"
                                });

                            showDialog(
                              context: context,
                              builder: (context) {
                                return Dialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(40)),
                                  elevation: 16,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: Colors.lightGreen),
                                    child: ListView(
                                      shrinkWrap: true,
                                      children: <Widget>[
                                        SizedBox(height: 20),
                                        Center(
                                            child: Text('Check your saved Items',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold))),
                                        SizedBox(height: 20),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          } on Exception {}
                        },
                      ),
                      IconButton(
                        alignment: Alignment.topLeft,
                        icon: Icon(Icons.share),
                        onPressed: () {},
                      ),
                      FlatButton(
                          onPressed: () {},
                          textTheme: ButtonTextTheme.primary,
                          child: Text(
                            'Check it out !',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          )),
                    ],
                  ),
                ])),
            Card(
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: Image.asset('assets/images/Salle 6.jpg'),
                    subtitle: Text("Dates : Fev 2022 -> Oct 2022"),
                    title: Text("Royal Wedding Plaza",
                        style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Experience the highest form of luxury and become a true King & Queen'),
                  ),
                  ButtonBar(
                    alignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: Icon(FontAwesome.heart),
                        color: Colors.black,
                        onPressed: () {},
                      ),
                      IconButton(
                        alignment: Alignment.topLeft,
                        icon: Icon(Icons.share),
                        onPressed: () {},
                      ),
                      FlatButton(
                          onPressed: () {},
                          textTheme: ButtonTextTheme.primary,
                          child: Text(
                            'Check it out !',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          )),
                    ],
                  ),
                ])),
            Card(
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: Image.asset('assets/images/salle2.jpg'),
                    subtitle: Text("Dates : Jan 2022 -> Mar 2022"),
                    title: Text("Sophie Wedding Venue",
                        style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Live the night of your life in one of the most luxurious wedding venues!'),
                  ),
                  ButtonBar(
                    alignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: Icon(FontAwesome.heart),
                        color: Colors.black,
                        onPressed: () {},
                      ),
                      IconButton(
                        alignment: Alignment.topLeft,
                        icon: Icon(Icons.share),
                        onPressed: () {},
                      ),
                      FlatButton(
                          onPressed: () {},
                          textTheme: ButtonTextTheme.primary,
                          child: Text(
                            'Check it out !',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          )),
                    ],
                  ),
                ])
            ),
            Card(
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: Image.asset('assets/images/Salle5.jpg'),
                    subtitle: Text("Dates : May 2022 -> Aug 2022"),
                    title: Text("Basboussa Outdoors",
                        style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Nothing is more romantic than an outdoor wedding with an open view of the Sea'),
                  ),
                  ButtonBar(
                    alignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: Icon(FontAwesome.heart),
                        color: Colors.black,
                        onPressed: () {},
                      ),
                      IconButton(
                        alignment: Alignment.topLeft,
                        icon: Icon(Icons.share),
                        onPressed: () {},
                      ),
                      FlatButton(
                          onPressed: () {},
                          textTheme: ButtonTextTheme.primary,
                          child: Text(
                            'Check it out !',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          )),
                    ],
                  ),
                ])
            )
          ],
        ),
      ),
    );
  }
}