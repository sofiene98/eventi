import 'dart:convert';

//import 'package:email_validator/email_validator.dart';
import 'package:eventiii/view/profile_screen.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

import 'Sign_Up.dart';
import '../viewmodel/User.dart';
import 'package:http/http.dart' as http;

import 'home.dart';
import '../main.dart';








class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  late String password="";
  late String username="";
  late bool statePassword=true;
  String errormsgg="";


  late Future<User> user;




  Future<User>  signIn(String mail,String password,context) async {
    String url="http://10.0.2.2:5000/auth?mail="+mail+"&password="+password;
    print("hhhh");




    final response = await http.get(Uri.parse(url),headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",

    });


    // If the server did return a 200 OK response,
    // then parse the JSON.
    //print(response.body);

    // print(await http.read(Uri.parse(url)));


    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      //print(jsonDecode(response.body));
      var res=jsonDecode(response.body);

      int idSession=res['id'];
      print(idSession);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> Eventi() ));
      return User.fromJson(jsonDecode(response.body));

    }
    else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> SignIn()));
      errormsgg="Username Or password are incorrects";
      print(errormsgg);
      throw Exception('Utilisateur non authentifié');
    }

  }

  @override
  void initState() {
    super.initState();
    errormsgg="";
    //user=signIn("gharbisofiene98@gmail.com","sofiene",context);
    //print("heeeyyyy");
    //print(user);



  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Stack(
              children: [


                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0XFF75CFCF),
                ),
                const TopSignIn(),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.10,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.9,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color(0xFFF8F9FA),
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(45),
                            topRight: Radius.circular(45))),
                    child: Form(
                      child: ListView(
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Container(
                          //   height: 250,
                          //   width: MediaQuery.of(context).size.width * 0.8,
                          //   margin: EdgeInsets.only(
                          //       left: MediaQuery.of(context).size.width * 0.09),
                          //   child: Image.asset("assets/images/Salle.png"),
                          // ),
                          Padding(padding: EdgeInsets.all(20.0)),
/*
                          FutureBuilder<User>(
                            future: user,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                return Text(snapshot.data!.mail);
                              } else if (snapshot.hasError) {
                                return Text('${snapshot.error}');
                              }

                              // By default, show a loading spinner.
                              return const CircularProgressIndicator();
                            },
                          ),

 */
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: TextFormField(
                              validator: EmailValidator(errorText: 'your email is incorrect') ,
                              //onTap: ,
                              keyboardType: TextInputType.text ,
                              onChanged : (value){
                                    setState(() {
                                        username=value;
                                        //print(username);
                                    });
                              },
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(10.0),
                                labelText: 'Email',

                                //errorText: 'Error message',
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: TextFormField(
                              obscureText: statePassword,
                              onChanged : (value){
                                setState(() {
                                  password=value;
                                  //print(password);
                                });
                              },
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(10.0),
                                  labelText: 'Password',
                                  //errorText: 'Error message',
                                  border: OutlineInputBorder(),
                                  suffixIcon: IconButton(
                                    icon: Icon(Icons.remove_red_eye),
                                    onPressed: () {
                                      setState(() {
                                        statePassword=!statePassword;
                                      });
                                    },)
                              ),
                            ),
                          ),

                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child:Text(errormsgg,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 10,
                                          )
                      ),
                    ),


                          InkWell(

                            onTap: () async {

                              //Navigator.pushNamed(context, 'HomePage');
                              setState(() {
                                if(username.length ==0 || password.length==0){
                                    errormsgg="Username or password are empty";
                                                                             }
                                else if(username.length >0 && password.length >0) {
                                  try {
                                    user = signIn(username, password, context);
                                    errormsgg = errormsgg;
                                  } on Exception {

                                                }
                                                                                    }
                              });
                             // await HttpService.login(username,password,context);
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height * 0.07,
                              margin: const EdgeInsets.only(left: 20, right: 20),
                              decoration: BoxDecoration(
                                  color: Color(0XFF75CFCF),
                                  borderRadius:
                                  const BorderRadius.all(Radius.circular(30))),
                              child: Center(
                                child: Text(
                                  "Sign In",
                                  style: TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xFFF8F9FA)),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

              ]
          ),
        ));
  }
}


class TopSignIn extends StatelessWidget {
  const TopSignIn({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AppBar(
        backgroundColor: Color(0XFF75CFCF),
        title: Text('LogIn to your account',
          style: TextStyle(
              fontWeight: FontWeight.w900,
              fontSize: 25,
              letterSpacing: 0.5
          ),

        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
