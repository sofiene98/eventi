import 'package:flutter/material.dart';


class Transport extends StatefulWidget {
  const Transport({Key? key}) : super(key: key);

  @override
  _TransportState createState() => _TransportState();
}

class _TransportState extends State<Transport> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        'First Typee ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    //First Type
                    Divider(
                      height: 10,
                      indent: 15.0,
                      endIndent: 15.0,
                      thickness: 3.5,
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                        Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/Transport 3.jpg'),
                            title: Text("MHK Luxery Cars",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, 'HomePage');
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Second Type ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Divider(
                      height: 10,
                      indent: 15.0,
                      endIndent: 15.0,
                      thickness: 3.5,
                    ),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                        Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/Transport 4.jpg'),
                            title: Text("AD Transportation",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, 'HomePage');
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ])),
                    Card(
                        margin: EdgeInsets.all(0.0),
                        child:
                        Column(mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(10.0),
                            leading: Image.asset('assets/images/Transport 5.jpg'),
                            title: Text("Galaxy Lumousine",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, 'HomePage');
                                  },
                                  child: Text(
                                    'Add to Cart',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ],
                          ),
                        ]))
                  ],
                ),
              ),
            ),
            TextButton.icon(
                onPressed: () {

                  Navigator.pushNamed(context, 'AddOffer');
                },
                icon: Icon(Icons.add),
                label: Text(
                  'Add a new offer',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                ))
          ],
        ),
      ),
    );
  }
}
