import 'dart:convert';

import 'package:eventiii/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import '../viewmodel/Album.dart';
import '../utils/CustomTextStyle.dart';
import '../utils/CustomUtils.dart';
import 'package:http/http.dart' as http;

//import 'CheckOutPage.dart';

Future<Album> fetchAlbum() async {
  final response = await http
      .get(Uri.parse('https://jsonplaceholder.typicode.com/albums/1'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return Album.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  late List<String> nomAnnonce = [];
  late List<String> categoriee = [];
  late List<double> prix = [];
  late int lengthres = 0;

  late double total = 0;

  int _currentIndex = 0;

  Future<void> fetchCommands() async {
    String url = "http://10.0.2.2:5000/getAnnonceSauv?idSession=6&mycommands=1";

    print(url);
    final response = await http.get(Uri.parse(url), headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*"
    });

    print(response.body.length);
    setState(() {
      lengthres = response.body.length;
    });


    try {
      for (int i = 0; i < response.body.length; i++) {
        var nomAnnoncee = json.decode(response.body)[i]['nom_annonce'];
        var categorie = json.decode(response.body)[i]['categorie'];
        var price = json.decode(response.body)[i]['prix'];
        total += price;

        setState(() {
          prix.add(price);
          nomAnnonce.add(nomAnnoncee);
          categoriee.add(categorie);
        });
      }
    } on Exception{

    }
  }

  @override
  void initState() {
    super.initState();

    try {
      fetchCommands();
    } on Exception {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.grey.shade100,
      // appBar: AppBar(
      //   elevation: 0.0,
      //   title: Text('Cart',
      //     style: TextStyle(
      //         fontSize: 20,
      //         fontWeight: FontWeight.bold
      //     ),
      //   ),
      //   leading: Builder(
      //     builder: (BuildContext context) {
      //       return IconButton(
      //           onPressed: () {
      //             Navigator.pop(context);
      //           },
      //           icon: Icon(Icons.arrow_back));
      //     },
      //   ),
      //   backgroundColor: Color(0XFF75CFCF),
      // ),
      // bottomNavigationBar: BottomNavigationBar(
      //   //mouseCursor: SystemMouseCursor._(kind: 'click') ,
      //   type: BottomNavigationBarType.fixed,
      //   currentIndex: _currentIndex,
      //   backgroundColor: Color(0XFF75CFCF),
      //   selectedItemColor: Color(0xFFFFFBFA),
      //   unselectedItemColor: Color(0xFFFFFBFA).withOpacity(0.60) ,
      //   onTap: (value) {
      //     setState(() {
      //       _currentIndex=value;
      //       print(_currentIndex);
      //       //screens[_currentIndex];
      //
      //     });
      //   },
      //   items: [
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.home),
      //         label: 'Home'
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.format_list_bulleted),
      //         label: 'Categories'
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.games),
      //         label: 'Games'
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.shopping_cart),
      //         label: 'Cart'
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.person_outline),
      //       label: 'Profile',
      //     ),
      //   ],
      // ),
      body: Builder(
        builder: (context) {
          return ListView(
            children: <Widget>[createCartList(), footer(context)],
          );
        },
      ),
    );
  }

  footer(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 30),
                child: Text(
                  "Total",
                  style: CustomTextStyle.textFormFieldMedium
                      .copyWith(color: Colors.grey, fontSize: 12),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 30),
                child: Text(
                  "\$ ${total}",
                  style: CustomTextStyle.textFormFieldBlack
                      .copyWith(color: Color(0XFF75CFCF), fontSize: 14),
                ),
              ),
            ],
          ),
          Utils.getSizedBox(height: 8, width: 0.0),
          RaisedButton(
            onPressed: () async {
              try {
                String url = "http://10.0.2.2:5000/sendDevis?idSession=6";
                print(url);
                final response = await http.get(Uri.parse(url), headers: {
                  "Accept": "application/json",
                  "Access-Control-Allow-Origin": "*"
                });

                showDialog(
                  context: context,
                  builder: (context) {
                    return Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40)),
                      elevation: 16,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.lightGreen),
                        child: ListView(
                          shrinkWrap: true,
                          children: <Widget>[
                            SizedBox(height: 20),
                            Center(
                                child: Text('Check your email',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold))),
                            SizedBox(height: 20),
                          ],
                        ),
                      ),
                    );
                  },
                );
              } on Exception {}

              // Navigator.push(context,
              //     new MaterialPageRoute(builder: (context) => Eventi() ));
            },
            color: Color(0XFF75CFCF),
            padding: EdgeInsets.only(top: 12, left: 60, right: 60, bottom: 12),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(24))),
            child: Text(
              "Devis",
              style: CustomTextStyle.textFormFieldSemiBold
                  .copyWith(color: Colors.white),
            ),
          ),
          Utils.getSizedBox(height: 8, width: 0.0),
        ],
      ),
      margin: EdgeInsets.only(top: 16),
    );
  }

  createSubTitle() {
    return Container(
      alignment: Alignment.topLeft,
      child: Text(
        "Total(3) Items",
        style: CustomTextStyle.textFormFieldBold
            .copyWith(fontSize: 12, color: Colors.grey),
      ),
      margin: EdgeInsets.only(left: 12, top: 4),
    );
  }

  createCartList() {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemBuilder: (context, position) {
        // base64, nomAnnonce , prix

        return createCartListItem(
            nomAnnonce[position], prix[position], categoriee[position]);
      },
      itemCount: nomAnnonce.length,
    );
  }

  createCartListItem(String nomAnnonce, double prix, String categorie) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Column(
            children: [
              Row(
                children: <Widget>[
                  Container(
                    margin:
                        EdgeInsets.only(right: 8, left: 8, top: 8, bottom: 8),
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                      //  color: Colors.blue.shade200,
                        image: DecorationImage(
                            image: AssetImage("assets/images/S4.png"))),
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(right: 8, top: 4),
                            child: Text(
                              nomAnnonce,
                              maxLines: 2,
                              softWrap: true,
                              style: CustomTextStyle.textFormFieldSemiBold
                                  .copyWith(fontSize: 14),
                            ),
                          ),
                          Utils.getSizedBox(height: 6, width: 0.0),
                          Text(
                            categorie,
                            style: CustomTextStyle.textFormFieldRegular
                                .copyWith(color: Colors.grey, fontSize: 14),
                          )
                        ],
                      ),
                    ),
                    flex: 100,
                  ),
                ],
              ),
              Divider(
                thickness: 2.0,
                indent: 10.0,
                endIndent: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(30.0, 0.0, 0.0, 0.0),
                    child: Text(
                      "\$  ${prix}",
                      style: CustomTextStyle.textFormFieldBlack
                          .copyWith(color: Color(0XFF75CFCF)),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          TextButton(
                              onPressed: () {},
                              child: Text(
                                'Change plan',
                                style: TextStyle(
                                    decoration: TextDecoration.underline),
                              )),
                          IconButton(
                              onPressed: () {},
                              icon: Icon(FontAwesome5.trash_alt)),
                        ],
                      ))
                ],
              ),
            ],
          ),
        )
      ],
    );
  }
}
