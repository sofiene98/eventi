import 'package:eventiii/view/saved_offers.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:eventiii/main.dart';

import 'Games.dart';
import 'cart.dart';
import 'categories.dart';
import 'home.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //   appBar: AppBar(
      //     elevation: 0.0,
      //     title: Text('Profile',
      //       style: TextStyle(
      //           fontSize: 20,
      //           fontWeight: FontWeight.bold
      //       ),
      //     ),
      //     leading: Builder(
      //       builder: (BuildContext context) {
      //         return IconButton(
      //             onPressed: () {
      //               Navigator.pop(context);
      //             },
      //             icon: Icon(Icons.arrow_back));
      //       },
      //     ),
      //     backgroundColor: Color(0XFF75CFCF),
      //   ),
      //   bottomNavigationBar: BottomNavigationBar(
      //   //mouseCursor: SystemMouseCursor._(kind: 'click') ,
      //   type: BottomNavigationBarType.fixed,
      //   currentIndex: _currentIndex,
      //   backgroundColor: Color(0XFF75CFCF),
      //   selectedItemColor: Color(0xFFFFFBFA),
      //   unselectedItemColor: Color(0xFFFFFBFA).withOpacity(0.60) ,
      //   onTap: (value) {
      //     setState(() {
      //       _currentIndex=value;
      //       print(_currentIndex);
      //       //screens[_currentIndex];
      //
      //     });
      //   },
      //   items: [
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.home),
      //         label: 'Home'
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.format_list_bulleted),
      //         label: 'Categories'
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.games),
      //         label: 'Games'
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.shopping_cart),
      //         label: 'Cart'
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.person_outline),
      //       label: 'Profile',
      //     ),
      //   ],
      // ),
      body: SafeArea(
          child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height *0.189,
            color: Color(0XFF75CFCF),
            child: Container(
                width: double.infinity,
                height: 80,
                child: Container(
                  alignment: Alignment(0.0, 10.0),
                  child: CircleAvatar(
                    radius: 90.0,
                    backgroundColor: Colors.white,
                    child: CircleAvatar(
                      backgroundImage:
                          AssetImage('assets/images/Mahdi Hadj Kacem.jpg'),
                      radius: 80.0,
                    ),
                  ),
                )
            ),
          ),
          SizedBox(height: 5),
          Text(
            'Mahdi Hadj Kacem',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          Container(
            height: MediaQuery.of(context).size.height *0.5,
            color: Color(0XFF75CFCF).withOpacity(0.60),
            child: Expanded(
              child: SingleChildScrollView(
                child:
                  Container(
                    //height: MediaQuery.of(context).size.height,

                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                      children: <Widget>[
                        SizedBox(
                          child: Column(
                            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: [
                                  Row(
                                    children: [
                                      IconButton(
                                        icon: Icon(FontAwesome.cog_alt),
                                        color: Colors.black,
                                        onPressed: () {
                                          Navigator.pushNamed(context, 'EditProfile');
                                        },
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(20.0),
                                        child: Text(
                                          'Edit profile',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold, fontSize: 18),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],

                              ),
                              Divider(
                                height: 5,
                                color: Colors.black,
                                thickness: 2.0,
                              ),
                              Row(
                                children: [
                                  IconButton(
                                    icon: Icon(FontAwesome.shopping_basket),
                                    color: Colors.black,
                                    onPressed: () {
                                      //Navigator.pushNamed(context, 'CartPage');
                                    },
                                  ),
                                  InkWell(
                                    onTap: () {
                                      new CartPage();
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Text(
                                        'Mes Commandes',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Divider(
                                height: 5,
                                color: Colors.black,
                                thickness: 2.0,
                              ),
                              Row(
                                children: [
                                  IconButton(
                                    icon: Icon(FontAwesome.heart),
                                    color: Colors.black,
                                    onPressed: () {

                                      Navigator.push(context, new MaterialPageRoute(builder: (context) => SavedOffers() ));
                                      //SavedOffers
                                    },
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(1.0),
                                    child: Text(
                                      'Offres sauvegardées',
                                      //textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold, fontSize: 18),
                                    ),
                                  ),
                                ],
                              ),
                              // Divider(
                              //   height: 5,
                              //   color: Colors.black,
                              //   thickness: 2.0,
                              // ),
                              // Row(
                              //   children: [
                              //     IconButton(
                              //       icon: Icon(Entypo.logout),
                              //       color: Colors.black,
                              //       onPressed: () {},
                              //     ),
                              //     Padding(
                              //       padding: const EdgeInsets.all(20.0),
                              //       child: Text(
                              //         'Déconnexion',
                              //         style: TextStyle(
                              //             fontWeight: FontWeight.bold, fontSize: 18),
                              //       ),
                              //     ),
                              //   ],
                              // ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),

              ),
            ),
          )
        ],
      )),
    );
  }
}
