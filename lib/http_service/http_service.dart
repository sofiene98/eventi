import 'dart:convert';

import 'package:eventiii/view/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import '../view/sign_in.dart';

class HttpService {
  static final _client = http.Client();

  static var _loginUrl = Uri.parse('http://192.168.1.4:5000/flutter');

  static var _registerUrl = Uri.parse('http://localhost:5000/register');

  static login(username, pass, context) async {


    print("hellloooo");
//gharbisofiene98@gmail.com

       final response = await post(_loginUrl,
        headers: {
          "Access-Control-Allow-Origin": "*",

          'Content-Type': 'application/json; charset=UTF-8'},
        body: jsonEncode(<String, String>{
          'uname': username,
          'passw': pass,
        }),

      );



    print("hellloooo afterr");


    if (response.statusCode == 200) {
      print(jsonDecode(response.body));
      var json = jsonDecode(response.body);
      print(json['status']);
      if (json['status'] == 'Login Sucessfully') {
        await EasyLoading.showSuccess(json['status']);
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => Home()));
      } else {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => Home()));
        EasyLoading.showError(json['status']);
      }
    } else {
      await EasyLoading.showError(
          "Error Code : ${response.statusCode.toString()}");
    }
  }

  static register(username, email, pass, context) async {
    http.Response response = await _client.post(_registerUrl, body: {
      'uname': username,
      'mail': email,
      'passw': pass,
    });

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      if (json['status'] == 'username already exist') {
        await EasyLoading.showError(json['status']);
      } else {
        await EasyLoading.showSuccess(json['status']);
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => SignIn()));
      }
    } else {
      await EasyLoading.showError(
          "Error Code : ${response.statusCode.toString()}");
    }
  }
}