
## EVENTI

Il s'agit d'une application mobile qui facilite l'organisation d'un **événement/mariage** en proposant ces services :

1. Disponibilité sur le marché

2. Offres promotionnels pour notre clientèle

3. Un système de fidélité

4. Apporter la liberté dans le choix des différents composants de son événement

**Pour visualiser le prototype, vous pouvez cliquer sur ce lien:**

[https://www.figma.com/proto/cYWKPwbQsoHNLl8KveasxO/Front-page-ver_1?node-id=40%3A115&scaling=scale-down&page-id=0%3A1&starting-point-node-id=41%3A315&show-proto-sidebar=1]

**Ce lien drive contient le vidéo démonstration ainsi que le fichier .apk:**

[https://drive.google.com/drive/folders/1McXXsPHIQZFK69oLFIvB_fkRo1jXDL_W?fbclid=IwAR0iZtAJfpgJ47o6J_WLV620MEh8ALwRSCwXVmFDq31-3g3sl_MRxdKnCCw]